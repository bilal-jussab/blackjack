import random

class Deck(object):

    cards = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
    suits = ['Hearts', 'Diamonds', 'Spades', 'Clubs']

    def __init__(self):
        self.deck = self.get_cards()

    def get_cards(self):
        return [(card, suit) for card in self.cards for suit in self.suits]

    def deal_card(self):
        deck = self.deck
        card = random.choice(deck)
        deck.remove(card)
        # print len(deck)
        return card

class BlackJack(object):

    dealer_hand = []
    player_hand = []
    player_finished = False
    dealer_finished = False

    card_values = {
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        '10': 10,
        'J': 10,
        'Q': 10,
        'K': 10,
        'A': 11,
    }
    
    def __init__(self):
        self.deck = Deck()

    def deal_starting_cards(self):
        while len(self.dealer_hand) < 2:
            self.dealer_hand.append(self.deck.deal_card())
        # print "Dealer ".format(self.dealer_hand)
        while len(self.player_hand) < 2:
            self.player_hand.append(self.deck.deal_card())
        # print "Player ".format(self.player_hand)
        print "Initial Hand:"
        print "Dealer {}".format(self.dealer_hand)
        print "Player {}".format(self.player_hand)

    def hand_value(self, hand):
        return [self.card_values[card[0]] for card in hand]

    def sum_cards(self):
        dealer_sum = sum(self.hand_value(self.dealer_hand))
        player_sum = sum(self.hand_value(self.player_hand))
        return {"dealer": dealer_sum, "player": player_sum}

    def is_dealer_bust(self):
        if self.sum_cards()['dealer'] > 21:
            return True
        return False

    def is_player_bust(self):
        if self.sum_cards()['player'] > 21:
            return True
        return False

    def blackjack_or_bust(self):
        sums = self.sum_cards()
        dealer = sums['dealer']
        player = sums['player']
        
        print "\n" 
        print "Dealer has: {} - Player has: {}".format(dealer, player)
        print "\n" 

        if self.is_dealer_bust() and self.is_player_bust():
            print "*** Haha you both Lost ***"
        elif self.is_dealer_bust():
            print '*** Dealer is Bust, Player Wins ***'
        elif self.is_player_bust():
            print '*** Player is Bust, Dealer Wins ***'
        elif not self.is_player_bust() and not self.player_finished:
            self.player_next_move(player)
        elif self.player_finished and not self.dealer_finished:
            self.dealer_next_move(dealer)
        elif self.player_finished and self.dealer_finished:
            if player > dealer:
                print '*** Player beats Dealer ***'
            elif player < dealer:
                print '*** Dealer beats Player ***'

    def hit(self):
        card = self.deck.deal_card()
        self.player_hand.append(card)
        print "Player dealt: {}".format(card)

    def dealer_next_move(self, dealer):
        print "Dealer has {} with Hand {}".format(dealer, self.dealer_hand)
        if dealer < 17:
            card = self.deck.deal_card()
            self.dealer_hand.append(card)
            print "Dealer dealt: {}".format(card)
        else:
            self.dealer_finished = True
        
        self.blackjack_or_bust()
        

    def player_next_move(self, player):
        valid_decisions = ['0', '1']

        print "Player you have {} with Hand {}".format(player, self.player_hand)
        print "Do you want to Stick or Hit?" 
        print "Type 0 for Stick, 1 for Hit"
        decision = raw_input()
     
        while (decision not in valid_decisions):
            print "Sorry not valid"
            decision = raw_input()

        if decision == '1':
            self.hit()
        elif decision == '0':
            self.player_finished = True
        
        self.blackjack_or_bust()

    def run(self):
        self.deal_starting_cards()
        self.blackjack_or_bust()

game = BlackJack()
game.run()
